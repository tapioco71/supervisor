;;;; package.lisp

(defpackage #:supervisor
  (:use #:cl)
  (:export #:get-current-state
	   #:set-current-state
	   #:get-lcd-device
	   #:set-lcd-device
	   #:get-timer
	   #:add-timer
	   #:remove-timer
	   #:start
	   #:stop
	   #:start-timer
	   #:stop-timer
	   #:running-p
	   #:add-state-handler
	   #:push-event
	   #:assert-log))

