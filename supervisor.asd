;;;; supervisor.asd

(asdf:defsystem #:supervisor
  :description "Supervisor"
  :author "Angelo Rossi <angelo.rossi.homelab@gmail.com>"
  :license "GPL3"
  :depends-on (#:bordeaux-threads
	       #:cl-log
	       #:trivial-timers
	       #:state-machine
               #:i2c
	       #:mcp23017
               #:lcd-i2c
	       #:max11615)
  :serial t
  :components ((:file "package")
               (:file "supervisor")))

