;;;; supervisor.lisp

(in-package #:supervisor)

;;; "supervisor" goes here. Hacks and glory await!

;;
;; LCD state machine class.
;;

(defclass lcd-state-machine-descriptor (state-machine:state-machine-descriptor)
  ((timers-table
    :initarg :timers-table
    :initform nil
    :accessor timers-table)
   (lcd-device-object
    :initarg :lcd-device-object
    :initform nil
    :accessor lcd-device-object)))

(defgeneric get-timer (object &rest parameters &key name))
(defgeneric add-timer (object &rest parameters &key name handler))
(defgeneric remove-timer (object &rest parameters &key name))
(defgeneric start-timer (object &rest parameters &key name timeout))
(defgeneric stop-timer (object &rest parameters &key name))
(defgeneric get-lcd-device (object))
(defgeneric set-lcd-device (object &rest parameters &key lcd-device-object))

(defparameter *minimum-log-level* 0)
(defparameter *backlight-off-timeout* 10.0)
(defparameter *power-down-timeout* 20.0)
(defparameter *mcp23017-device-object* nil)
(defparameter *lcd-i2c-device-object* nil)
(defparameter *log-file-pathname* #p"supervisor.log")
(defparameter *lcd-state-machine-object* nil)

;;
;; Macros.
;;

(defmacro assert-log ((message &rest parameters &key level) &body body)
  (declare (ignorable parameters level))
  (let ((return-value (gensym "return-value-"))
	(log-message (gensym "log-message-")))
    `(let ((,log-message (format nil "~a: " ,message))
	   (,return-value (progn
			    ,@body)))
       (when (> ,level *minimum-log-level*)
	 (if ,return-value
	     (setq ,log-message (concatenate 'string ,log-message "OK~&"))
	     (setq ,log-message (concatenate 'string ,log-message "FAILED~&")))
	 (cl-log:log-message :info (format nil ,log-message)))
       ,return-value)))

;;
;; Functions for LCD state machine class.
;;

(defun make-lcd-state-machine (&rest parameters &key (name (symbol-name (gensym "state-machine-"))) (initial-state :idle) (lcd-device-object nil))
  (declare (ignorable parameters name initial-state backlight-off-timeout power-down-timeout lcd-device-object))
  (let ((object (make-instance 'lcd-state-machine-descriptor
			       :name name
			       :lock-object (bordeaux-threads:make-lock (symbol-name (gensym "lcd-state-machine-lock-")))
			       :handlers-table (make-hash-table)
			       :timers-table (make-hash-table :test #'equal)
			       :lcd-device-object lcd-device-object)))
    (when initial-state
      (state-machine:push-event object initial-state))
    object))

;;
;; Methods for LCD state machine class.
;;

(defmethod get-current-state ((object lcd-state-machine-descriptor))
  (current-state object))

(defmethod set-current-state ((object lcd-state-machine-descriptor) state)
  (bordeaux-threads:with-lock-held ((state-machine::lock-object object))
    (setf (state-machine::current-state object) state)))

(defmethod get-lcd-device ((object lcd-state-machine-descriptor))
  "Get the LCD device object."
  (lcd-device-object object))

(defmethod set-lcd-device ((object lcd-state-machine-descriptor) &rest parameters &key lcd-device-object)
  "Set the LCD device object."
  (declare (ignorable parameters lcd-device-object))
  (bordeaux-threads:with-lock-held ((state-machine::lock-object object))
    (setf (lcd-device-object object) lcd-device-object)))

(defmethod get-timer ((object lcd-state-machine-descriptor) &rest parameters &key name)
  "Get a timer from the state machine."
  (declare (ignorable parameters name))
  (multiple-value-bind (timer-object status)
      (gethash name (timers-table object))
    (when status
      timer-object)))

(defmethod add-timer ((object lcd-state-machine-descriptor) &rest parameters &key (name (symbol-name (gensym "lcd-state-machine-timer-"))) (handler nil))
  "Add a timer with the timeout handler to the state machine."
  (declare (ignorable parameters name handle))
  (multiple-value-bind (timer-object status)
      (gethash name (timers-table object))
    (when (and (null status)
	       (functionp handler))
      (bordeaux-threads:with-lock-held ((state-machine::lock-object object))
	(setf (gethash name (timers-table object)) (trivial-timers:make-timer handler
									      :name name
									      :thread t))))))

(defmethod remove-timer ((object lcd-state-machine-descriptor) &rest parameters &key name)
  "Remove a timer from the state machine."
  (declare (ignorable parameters name))
  (multiple-value-bind (timer-object status)
      (gethash name (timers-table object))
    (when status
      (bordeaux-threads:with-lock-held ((state-machine::lock-object object))	
	(remhash name (timers-table-object))))))

(defmethod start-timer ((object lcd-state-machine-descriptor) &rest parameters &key name timeout)
  "Schedule a timer, with a specified timeout value in seconds, inside the state machine."
  (declare (ignorable parameters name timeout))
  (multiple-value-bind (timer-object status)
      (gethash name (timers-table object))
    (when status
      (bordeaux-threads:with-lock-held ((state-machine::lock-object object))
	(trivial-timers:schedule-timer timer-object timeout))
      (trivial-timers:timer-scheduled-p timer-object))))

(defmethod stop-timer ((object lcd-state-machine-descriptor) &rest parameters &key name)
  "Stop a previously scheduled timer."
  (declare (ignorable parameters name))
  (multiple-value-bind (timer-object status)
      (gethash name (timers-table object))
    (when status
      (bordeaux-threads:with-lock-held ((state-machine::lock-object object))
	(when (trivial-timers:timer-scheduled-p timer-object)
	  (trivial-timers:unschedule-timer timer-object))
	(not (trivial-timers:timer-scheduled-p timer-object))))))

(defmethod running-p ((object lcd-state-machine-descriptor))
  (state-machine:running-p object))

(defmethod start ((object lcd-state-machine-descriptor) &rest parameters &key (initial-state :idle) &allow-other-keys)
  (declare (ignorable parameters initial-state))
  (state-machine:start object :initial-state initial-state))

(defmethod stop ((object lcd-state-machine-descriptor))
  (state-machine:stop object))

(defmethod add-state-handler ((object lcd-state-machine-descriptor) &rest parameters &key state handler &allow-other-keys)
  (declare (ignorable parameters state handler))
  (state-machine:add-state-handler object :state state :handler handler))

(defmethod push-event ((object lcd-state-machine-descriptor) event)
  (bordeaux-threads:with-lock-held ((state-machine::lock-object object))
    (if (consp (state-machine::events-queue object))
	(setf (cdr (last (state-machine::events-queue object))) (cons event nil))
	(setf (state-machine::events-queue object) (cons event nil)))
    (state-machine::events-queue object)))

;;
;; Function.
;;

(defun initialize-log (&rest parameters &key (log-file-pathname *log-file-pathname*))
  "Initialize messenger."
  (declare (ignorable parameters log-file-pathname))
  (setf (cl-log:log-manager) (make-instance 'cl-log:log-manager
					    :message-class 'cl-log:formatted-message))
  (cl-log:start-messenger 'cl-log:text-file-messenger
			  :filename log-file-pathname)
  (cl-log:log-message :info "Logger started."))

(defun deinitialize-log ()
  (cl-log:log-message :info "Logger stopped.")
  (setf (cl-log:logging-disabled (cl-log:log-manager)) t))

(defun run (&rest parameters &key (log-level 0) (backlight-off-timeout *backlight-off-timeout*) (power-down-timeout *power-down-timeout*))
  (unwind-protect
       (progn
	 (when (> log-level 0)
	   (initialize-log))
	 ;;
	 ;; Create mcp23017 device object.
	 ;;
	 (assert-log ("Creating mcp23017 device" :level 10)		     
	   (setq *mcp23017-device-object* (mcp23017:make-device :device-address #x20
								:device-file-pathname #p"/dev/i2c-1"
								:device-configuration (list :iocon (mcp23017:value->configuration #x00) 
											    :ports-and-pins-map mcp23017::*default-ports-and-pins-map*))))
	 ;;
	 ;; Create lcd device object.
	 ;;
	 (assert-log ("Creating LCD device" :level 10)
	   (setq *lcd-i2c-device-object* (lcd-i2c:make-device :i2c-device *mcp23017-device-object*)))
	 ;;
	 ;; Create state machine object.
	 ;;
	 (assert-log ("Creating LCD state machine" :level 10)
	   (setq *lcd-state-machine-object* (make-lcd-state-machine)))
	 (set-lcd-device *lcd-state-machine-object*
			 :lcd-device-object *lcd-i2c-device-object*)
	 (assert-log ("Adding IDLE state handle" :level 10)
	   (add-state-handler *lcd-state-machine-object*
			      :state :idle
			      :handler #'(lambda (object)
					   (assert-log ("IDLE state" :level 10)
					     t)
					   (push-event object :initialize))))
	 (assert-log ("Adding INITIALIZE state handler" :level 10)
	   (add-state-handler *lcd-state-machine-object*
			      :state :initialize
			      :handler #'(lambda (object)
					   (assert-log ("INITIALIZE state" :level 10)
					     t)
					   (unless (lcd-i2c:initialized-p (get-lcd-device object))
					     (lcd-i2c:initialize (get-lcd-device object) :columns 16 :rows 2)
					     (push-event object :backlight-on)))))
	 (assert-log ("Adding BACKLIGHT-ON state handler" :level 10)
	   (add-state-handler *lcd-state-machine-object*
			      :state :backlight-on
			      :handler #'(lambda (object)
					   (assert-log ("BACKLIGHT-ON state" :level 10)
					     t)
					   (lcd-i2c:backlight (get-lcd-device object))
					   (push-event object :display-home-message))))
	 (assert-log ("Adding DISPLAY-HOME-MESSAGE state handler" :level 10)		     
	   (add-state-handler *lcd-state-machine-object*
			      :state :display-home-message
			      :handler #'(lambda (object)
					   (assert-log ("DISPLAY-HOME-MESSAGE state" :level 10)
					     t)
					   (lcd-i2c:clear (get-lcd-device object))
					   (lcd-i2c:home (get-lcd-device object))			    
					   (lcd-i2c:format (get-lcd-device object) "Chicom S.r.l.")
					   (push-event object :start-timeout-timers))))
	 (assert-log ("Adding START-TIMEOUT-TIMERS state handler" :level 10)
	   (add-state-handler *lcd-state-machine-object*
			      :state :start-timeout-timers
			      :handler #'(lambda (object)
					   (declare (type lcd-state-machine-descriptor object))
					   (assert-log ("START-TIMEOUT-TIMERS state" :level 10)
					     t)
					   (assert-log ("Adding backlight off timer" :level 10)
					     (add-timer object
							:name "backlight-off-timer"
							:handler #'(lambda ()
								     (assert-log ("backlight off" :level 10)
								       t)
								     (push-event *lcd-state-machine-object* :backlight-off))))
					   (assert-log ("Adding power down timer" :level 10)
					     (add-timer object
							:name "power-down-timer"
							:handler #'(lambda ()
								     (assert-log ("power down" :level 10)
								       t)
								     (push-event *lcd-state-machine-object* :power-down))))
					   (assert-log ("Starting backlight off timer" :level 10)
					     (start-timer object
							  :name "backlight-off-timer"
							  :timeout backlight-off-timeout))
					   (assert-log ("Starting power down timer" :level 10)
					     (start-timer object
							  :name "power-down-timer"
							  :timeout power-down-timeout))
					   (push-event object :wait-for-event))))
	 (assert-log ("Adding BACKLIGHT-OFF state handler" :level 10)
	   (add-state-handler *lcd-state-machine-object*
			      :state :backlight-off
			      :handler #'(lambda (object)
					   (assert-log ("BACKLIGHT-OFF state" :level 10)
					     t)
					   (lcd-i2c:no-backlight (get-lcd-device object))
					   (push-event object :wait-for-event))))
	 (assert-log ("Adding POWER-DOWN state handler" :level 10)
	   (add-state-handler *lcd-state-machine-object*
			      :state :power-down
			      :handler #'(lambda (object)
					   (assert-log ("POWER-DOWN state" :level 10)
					     t)
					   (lcd-i2c:deinitialize (get-lcd-device object))
					   (push-event object :wait-for-event))))
	 (assert-log ("Adding WAIT-FOR-EVENT state handler" :level 10)
	   (add-state-handler *lcd-state-machine-object*
			      :state :wait-for-event
			      :handler #'(lambda (object)
					   (assert-log ("WAIT-FOR-EVENT state" :level 10)
					     t))))
	 (assert-log ("Starting LCD state machine" :level 10)
	   (start *lcd-state-machine-object*
		  :initial-state :idle))
	 (sleep 60))
    (progn
      (when *lcd-state-machine-object*
	(when (lcd-i2c:initialized-p (get-lcd-device *lcd-state-machine-object*))
	  (assert-log ("Deinitializing LCD device" :level 10)
	    (lcd-i2c:deinitialize (get-lcd-device *lcd-state-machine-object*)))
	  (when (running-p *lcd-state-machine-object*)
	    (assert-log ("Stopping LCD state machine" :level 10)
	      (stop *lcd-state-machine-object*)))))
      (when (> log-level 0)
	(deinitialize-log)))))
